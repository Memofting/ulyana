package com.company;

import com.company.bot.Bot;

public class Main {

    public static void main(String[] args) {

        Bot bot = new Bot(System.in, System.out, System.err);
        bot.run();
    }
}
