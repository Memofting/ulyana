package com.company.bot;
import com.company.communication.QuizCommunication;
import com.company.database.QuizDatabase;
import jdk.jshell.spi.ExecutionControl;

import java.io.*;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.zip.DataFormatException;

public class Bot {
    // Класс бота. Он реализует в себе интерфейс, который предоставляется
    // пользователю во время общения.
    InputStream in;
    PrintStream out;
    PrintStream err;

    QuizCommunication communication;

    public Bot(InputStream in, PrintStream out, PrintStream err) {
        // Инициализируется тремя потоками, с которых считываются и в
        // которые записываются ответы и ошибки программы.

        this.in = in;
        this.out = out;
        this.err = err;

        // в этом поле будет хранится пул способов коммуникации
        communication = new QuizCommunication();
    }


    public void run() {
        try {
            out.println(communication.getResponse(QuizCommunication.SAY_HELLO));
        } catch (Exception e)
        {
            err.println(e.getMessage());
        }

        while (true) {
            try{
                String request = readline();
                String response = communication.getResponse(request);

                out.println(response);
            }
            catch (ExecutionControl.NotImplementedException e){
                err.println(e.getMessage());
            }
            catch (DataFormatException e)
            {
                err.println(e.getMessage());
            }
        }
    }

    private String readline()
    {
        try {
            return new BufferedReader(new InputStreamReader(in)).readLine();
        }
        catch (IOException e){
            err.println(e.getMessage());
            return "";
        }
    }
}
