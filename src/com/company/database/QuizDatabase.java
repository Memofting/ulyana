package com.company.database;

import jdk.jshell.spi.ExecutionControl;

import java.util.HashMap;


public class QuizDatabase {
    // класс, который позволяет получать данные из базы данных для викторины

    public final String SAY_HELLO = "";
    public final String SAY_PLAY_GAME = "";

    public QuizDatabase()
    // Здесь устанавливается соединение с базой данных
    {}

    public String getHelp(String request)
            throws ExecutionControl.NotImplementedException
    {
        // Позволяет получить вопрос, который находится в базе данных по запросу request.
        // Содержание поля внутри request содержит форматированное описание для
        // поиска нужной записи.


        // "Начать игру" - формат request на начало игры

        throw new ExecutionControl.NotImplementedException("");
    }

    public HashMap<String, String> getQuestionsAndAnswers(String theme)
        throws  ExecutionControl.NotImplementedException
    {
        throw  new ExecutionControl.NotImplementedException("");
    }
}
